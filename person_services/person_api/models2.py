########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from qops_desktop import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime,
                           default=db.func.now(),
                           onupdate=db.func.now())


class Person(Base):
    __tablename__ = 'person'

    identifier = db.Column(db.String, nullable=False)
    first_name = db.Column(db.String, nullable=False)
    middle_name = db.Column(db.String, nullable=True)
    last_name = db.Column(db.String, nullable=False)
    date_of_birth = db.Column(db.String, nullable=True)
    date_of_death = db.Column(db.String, nullable=True)
    role_id = db.Column(db.Integer, db.ForeignKey('person_role.id'), nullable=False)

    def get_next_identifier():
        return 'PER00001'

class PersonRole(Base):
    __tablename__ = 'person_role'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)
    is_internal = db.Column(db.Integer)


class CommunicationMethod(Base):
    __tablename__ = 'communication_method'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class PersonCommunicationMethod(Base):
    __tablename__ = 'person_communication_method'

    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    communication_method_id = db.Column(db.Integer,
                                        db.ForeignKey('communication_method.id'))
    value = db.Column(db.String)
    usage = db.Column(db.String)


class PersonLocation(Base):
    __tablename__ = 'person_location'

    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'))
