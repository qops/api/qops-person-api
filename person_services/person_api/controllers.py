########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import models
from .models import Person
from .models import PersonRole

from .forms import PersonProfileForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_person = Blueprint('person', __name__, url_prefix='/person')

@mod_person.context_processor
def store():
    store_dict = { 'serviceName': 'Person',
                   'serviceDashboardUrl': url_for('person.dashboard'),
                   'serviceBrowseUrl': url_for('person.browse'),
                   'serviceNewUrl': url_for('person.new'),
                 }
    return store_dict

# Set the route and accepted methods
@mod_person.route('/', methods=['GET'])
def person():
    return render_template('person/person_dashboard.html')


@mod_person.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('person/person_dashboard.html')


@mod_person.route('/browse', methods=['GET'])
def browse():
    people = Person.query.with_entities(Person.id, Person.identifier, Person.first_name, Person.middle_name, Person.last_name, PersonRole.name.label('role_id')).join(
        PersonRole, Person.role_id == PersonRole.id).all()
    return render_template('person/person_browse.html', people=people)


@mod_person.route('/new', methods=['GET', 'POST'])
def new():
    person = Person()
    form = PersonProfileForm(request.form)
    form.role_id.choices = [(r.id, r.name) for r in PersonRole.query.all()]
    if request.method == 'POST':
        form.populate_obj(person)
        person.identifier = Person.get_next_identifier()
        db.session.add(person)
        db.session.commit()
        return redirect(url_for('person.browse'))
    return render_template('person/person_new.html', person=person, form=form)


@mod_person.route('/profile', methods=['GET', 'POST'])
@mod_person.route('/profile/<int:person_id>/profile', methods=['GET', 'POST'])
def profile(person_id=None):
    person = Person.query.get(person_id)
    form = PersonProfileForm(obj=person)
    form.role_id.choices = [(r.id, r.name) for r in PersonRole.query.all()]
    if request.method == 'POST':
        form = PersonProfileForm(request.form)
        form.populate_obj(person)
        db.session.add(person)
        db.session.commit()
        return redirect(url_for('person.browse'))
    return render_template('person/person_profile.html', person=person, form=form)


@mod_person.route('/view', methods=['GET', 'POST'])
@mod_person.route('/view/<int:person_id>/view', methods=['GET',
                                                          'POST'])
def person_view(person_id=None):
    #person = Person.query.get(person_id)
    form = PersonProfileForm(obj=person)
    form.role_id.choices = [(r.id, r.name) for r in PersonRole.query.all()]
    if request.method == 'POST':
        form = PersonProfileForm(request.form)
        form.populate_obj(person)
        db.session.add(person)
        db.session.commit()
        return redirect(url_for('person.browse'))
    return render_template('person/person_view.html', person=person,
                           form=form)


@mod_person.route('/profile/dashboard', methods=['GET'])
@mod_person.route('/profile/<int:person_id>/dashboard', methods=['GET'])
def person_dashboard(person_id=None):
    if person_id is not None:
        person = Person.query.get(person_id)
    else:
        person = None
    return render_template('person/person_single_dashboard.html', person=person)

